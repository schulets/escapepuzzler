﻿using UnityEngine;
using System.Collections;

public class EntStandController : MonoBehaviour {
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(-2.62f, 0.33f, 1.0f);
	}
	
    void OnMouseUp()
    {
        GameController.S.printText(text);
        print("There was nothing in the entertainment stand.");
    }
}
