﻿using UnityEngine;
using System.Collections;

public class LightsController : MonoBehaviour {

    public Sprite sprite1;
    public Sprite sprite2;
    public GameObject text;

    private SpriteRenderer spriteRenderer;

    void Start(){
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (GameController.S.lightsOn)
        {
            spriteRenderer.sprite = sprite1;
        }
        else
        {
            spriteRenderer.sprite = sprite2;
        }
        this.transform.position = new Vector3(-0.811f, 0.68f, 1f);
    }

    void OnMouseUp(){
        if (spriteRenderer.sprite == sprite1)
        {
            spriteRenderer.sprite = sprite2;
            GameController.S.lightsOn = false;
            GameController.S.GetTheLights();
        }
        else
        {
            spriteRenderer.sprite = sprite1;
            GameController.S.lightsOn = true;
            GameController.S.GetTheLights();
        }
    }
}
