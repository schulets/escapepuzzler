﻿using UnityEngine;
using System.Collections;

public class TableShelfController : MonoBehaviour {

    public Sprite sprite1;
    public Sprite sprite2;
    public GameObject text;
    public GameObject text2;

    private SpriteRenderer spriteRenderer;

    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (!GameController.S.hasScrewdriver)
        {
            spriteRenderer.sprite = sprite1;
        }
        else
        {
            spriteRenderer.sprite = sprite2;
        }
        this.transform.position = new Vector3(2.7f, -3.08f, 1f);
    }

    void OnMouseUp()
    {
        if (!GameController.S.hasScrewdriver)
        {
            ChangeSprite();

            GameController.S.printText(text);
        }
        else
        {

            //GameController.S.message = Instantiate(text2) as GameObject;
            GameController.S.printText(text2);
        }
    }

    void ChangeSprite(){
        if (spriteRenderer.sprite == sprite1){
            spriteRenderer.sprite = sprite2;
            Instantiate(GameController.S.scene1Objects[9]);
        }
    }
}
