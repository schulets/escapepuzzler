﻿using UnityEngine;
using System.Collections;

public class LampController : MonoBehaviour {
    public bool isBlackLight = false;
    public Sprite sprite1;
    public Sprite sprite2;
    public GameObject text;
    public GameObject text2;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        this.transform.position = new Vector3(2.67f, -0.87f, 0.9f);
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (GameController.S.blackLight)
        {
            spriteRenderer.sprite = sprite2;
        }
    }

    void OnMouseUp(){
        if (spriteRenderer.sprite == sprite1 && GameController.S.hasBlackLight)
        {
            spriteRenderer.sprite = sprite2;
            GameController.S.printText(text);
            print("blacklight placed in lamp");
            GameController.S.ChangeLampScene();
        }
        else
        {
            GameController.S.printText(text2);
            print("Nice lamp.");
        }
    }
	
}
