﻿using UnityEngine;
using System.Collections;

public class BrokenDoorController : MonoBehaviour {
    public GameObject text;
    public GameObject text2;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(2.04f, 0.51f, 1.0f);
	}

    void OnMouseUp()
    {
        if (!GameController.S.doorOpen && !GameController.S.hasCrowbar)
        {
            GameController.S.printText(text);
            //print("I wonder what is behind this door. I should look for a way to break it open.");
        }
        else if (!GameController.S.doorOpen && GameController.S.hasCrowbar)
        {
            GameController.S.doorOpen = true;
            Destroy(this.gameObject);
        }
        else
        {
            GameController.S.printText(text2);
            //print("The door is stuck open.");
        }
    }
}
