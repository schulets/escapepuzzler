﻿using UnityEngine;
using System.Collections;

public class ScrewController : MonoBehaviour {
    public GameObject text;

    void OnMouseUp()
    {
        Destroy(this.gameObject);
        GameController.S.hasScrewdriver = true;
    }
}
