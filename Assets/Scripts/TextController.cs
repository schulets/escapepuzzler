﻿using UnityEngine;
using System.Collections;

public class TextController : MonoBehaviour {
    private float timeAccessed;
    private float timeToDestroy;
	// Use this for initialization
	void Start () {
        timeAccessed = Time.time;
        timeToDestroy = Time.time + 2;
    }

    void Update()
    {
        if (Time.time > timeToDestroy)
        {
            Destroy(this.gameObject);
        }
    }
}
