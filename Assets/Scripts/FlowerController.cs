﻿using UnityEngine;
using System.Collections;

public class FlowerController : MonoBehaviour {
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(-3.58f, -2.2f, 1.0f);
	}
	
    void OnMouseUp()
    {
        GameController.S.printText(text);
        print("Pretty flowers.");
    }

    void Update()
    {
        //lights on
        if (GameController.S.doorOpen && GameController.S.sceneIndex == 6)
        {
            GameController.S.ChangeScene(6, 8);
        }
        //lights off
        else if (GameController.S.doorOpen && GameController.S.sceneIndex == 7)
        {
            GameController.S.ChangeScene(7, 9);
        }
    }
}
