﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {
    public GameObject winMessage;
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(-3.46f, 0.65f, 1f);
    }

	void OnMouseUp(){
        if (GameController.S.hasKey != true){
            GameController.S.printText(text);
            print("The Door is locked");
        }
        else
        {
            //win
            Instantiate(winMessage);
            winMessage.transform.position = new Vector3(0f, 0f, 0f);
        }
    }
}
