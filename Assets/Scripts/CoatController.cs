﻿using UnityEngine;
using System.Collections;

public class CoatController : MonoBehaviour {
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(-4.26f, -1.11f, 1.0f);
	}
	
    void OnMouseUp()
    {
        GameController.S.printText(text);
        print("Old coat. Looks like it's been through a lot.");
    }
}
