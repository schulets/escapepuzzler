﻿using UnityEngine;
using System;
using System.Collections;

public class GameController : MonoBehaviour {
    public GameObject[] scenes;
    public int sceneIndex;
    public GameObject currentScene;
    //scene1
    public GameObject currentPainting;
    public GameObject switchOn;
    public GameObject table;
    public GameObject lamp;
    public GameObject door;
    public GameObject painting;
    public GameObject arrowLeft;
    public GameObject arrowRight;
    public GameObject painting_zoom;
    public GameObject zoomed;
    public GameObject back;
    public GameObject backButton;
    public GameObject key;

    public GameObject[] scene1Objects;
    public GameObject[] scene2Objects;
    public GameObject[] scene3Objects;
    public GameObject[] scene4Objects;

    //buttons
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;
    public GameObject button7;
    public GameObject button8;
    public GameObject button9;
    public GameObject button0;
    public string combo;

    //scene2
    public GameObject couch;
    public GameObject clock;
    public GameObject entStand;
    public GameObject lampStand;

    //scene3
    public GameObject doorOld;
    public GameObject safe;
    public GameObject flowers;

    //scene4
    public bool closetOpen = false;
    public GameObject closet;
    public GameObject coat;
    public GameObject crowBar;

    //is broken door open
    public bool doorOpen = false;

    //is the safe opened
    public bool safeOpen = false;

    //lights on or off
    public bool lightsOn = true;

    //lights black or not
    public bool blackLight = false;

    //is painting zoomed up on
    public bool clicked;

    //items in inventory booleans
    public bool hasScrewdriver = false;
    public bool hasBlackLight = false;
    public bool hasKey = false;
    public bool hasCrowbar = false;


    public GameObject start_text;

    public static GameController S;

    // Use this for initialization
    void Start()
    {
        S = this;
        GameObject sceneStart = Instantiate(scenes[0]) as GameObject;
        sceneStart.transform.position = new Vector3(0f, 0f, 1f);
        currentScene = sceneStart;
        sceneIndex = 0;

        InstantiateObjects(sceneIndex);
        printText(start_text);
    }


    public GameObject message;
    public void printText(GameObject text)
    {
        
        message = Instantiate(text) as GameObject;
        message.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
    }

    
    void InstantiateObjects(int index)
    {
        if (index == 0 || index == 1 || index == 2 || index == 3)
        {
            arrowLeft = Instantiate(scene1Objects[0]) as GameObject;
            arrowRight = Instantiate(scene1Objects[1]) as GameObject;
            switchOn = Instantiate(scene1Objects[3]) as GameObject;
            table = Instantiate(scene1Objects[4]) as GameObject;
            lamp = Instantiate(scene1Objects[5]) as GameObject;
            door = Instantiate(scene1Objects[2]) as GameObject;
            painting = Instantiate(scene1Objects[6]) as GameObject;
        }
        else if (index == 4 || index == 5)
        {
            arrowLeft = Instantiate(scene2Objects[0]) as GameObject;
            arrowRight = Instantiate(scene2Objects[1]) as GameObject;
            couch = Instantiate(scene2Objects[5]) as GameObject;
            clock = Instantiate(scene2Objects[3]) as GameObject;
            entStand = Instantiate(scene2Objects[4]) as GameObject;
            lampStand = Instantiate(scene2Objects[6]) as GameObject;
        }
        else if (index == 6 || index == 7 || index == 8 || index == 9)
        {
            arrowLeft = Instantiate(scene3Objects[0]) as GameObject;
            arrowRight = Instantiate(scene3Objects[1]) as GameObject;
            flowers = Instantiate(scene3Objects[3]) as GameObject;
            if (!doorOpen)
            {
                doorOld = Instantiate(scene3Objects[2]) as GameObject;

            }
            else
            {
                safe = Instantiate(scene3Objects[4]) as GameObject;
            }
        }
        else if (index == 10 || index == 11)
        {
            arrowLeft = Instantiate(scene4Objects[0]) as GameObject;
            arrowRight = Instantiate(scene4Objects[1]) as GameObject;
            closet = Instantiate(scene4Objects[2]) as GameObject;
            coat = Instantiate(scene4Objects[3]) as GameObject;
            if (closetOpen && !hasCrowbar)
            {
                crowBar = Instantiate(scene4Objects[4]) as GameObject;
            }
        }
    }


    void DestroyObjects(int index)
    {
        if (index == 0 || index == 1 || index == 2 || index == 3)
        {
            Destroy(arrowLeft);
            Destroy(arrowRight);
            Destroy(switchOn);
            Destroy(table);
            Destroy(lamp);
            Destroy(door);
            Destroy(painting);
        }
        else if (index == 4 || index == 5)
        {
            Destroy(arrowLeft);
            Destroy(arrowRight);
            Destroy(clock);
            Destroy(couch);
            Destroy(lampStand);
            Destroy(entStand);
        }
        else if (index == 6 || index == 7 || index == 8 || index == 9)
        {
            Destroy(arrowLeft);
            Destroy(arrowRight);
            Destroy(flowers);
            if (!doorOpen)
            {
                Destroy(doorOld);
            }
            else
            {
                Destroy(safe);
            }
        }
        else if (index == 10 || index == 11)
        {
            Destroy(arrowLeft);
            Destroy(arrowRight);
            Destroy(closet);
            Destroy(coat);
            if (!hasCrowbar && closetOpen)
            {
                Destroy(crowBar);
            }
        }
    }

    public void ChangeScene()
    {
        Destroy(currentScene);
        DestroyObjects(sceneIndex);
        currentScene = Instantiate(scenes[sceneIndex]);
        currentScene.transform.position = new Vector3(0f, 0f, 1f);
        InstantiateObjects(sceneIndex);
    }

    public void ChangeScene(int oldIndex, int newIndex)
    {
        Destroy(currentScene);
        currentScene = Instantiate(scenes[newIndex]);
        currentScene.transform.position = new Vector3(0f, 0f, 1f);
        sceneIndex = newIndex;
        DestroyObjects(oldIndex);
        InstantiateObjects(newIndex);
//            if (!(newIndex == 0 || newIndex == 1 || newIndex == 2 || newIndex == 3))
 //           {
  //              lamp.transform.position = new Vector3(2.67f, -0.87f, 2f);
   //         }
    //        else
     //       {
      //          lamp.transform.position = new Vector3(2.67f, -0.87f, 0.9f);
       //     }
    }

    public void GetTheLights(){
        if (!lightsOn && !blackLight)
        {
            Destroy(currentScene);
            sceneIndex = 1;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
        else if (lightsOn && !blackLight)
        {
            Destroy(currentScene);
            sceneIndex = 0;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
        else if (lightsOn && blackLight){
            Destroy(currentScene);
            sceneIndex = 2;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
        else if (!lightsOn && blackLight){
            Destroy(currentScene);
            sceneIndex = 3;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
    }

    public void ChangeLampScene()
    {
        blackLight = true;
        if (lightsOn)
        {
            Destroy(currentScene);
            sceneIndex = 2;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
        else if (!lightsOn)
        {
            Destroy(currentScene);
            sceneIndex = 3;
            currentScene = Instantiate(scenes[sceneIndex]);
            currentScene.transform.position = new Vector3(0f, 0f, 1f);
        }
    }

    public void zoomPainting()
    {
        if (clicked)
        {
            zoomed = Instantiate(scene1Objects[7]) as GameObject;
            zoomed.transform.position = new Vector3(0f, 0f, 0f);
            backButton = Instantiate(scene1Objects[8]) as GameObject;
            backButton.transform.position = new Vector3(0f, 0f, -2f);
        }
    }

    public int count = 0;
    public GameObject zero;
    public GameObject one;
    public GameObject two;
    public GameObject three;
    public GameObject four;
    public GameObject five;
    public GameObject six;
    public GameObject seven;
    public GameObject eight;
    public GameObject nine;

    public void zoomSafe()
    {
        zoomed = Instantiate(scene3Objects[5]) as GameObject;
        zero = Instantiate(button0) as GameObject;
        one = Instantiate(button1) as GameObject;
        two = Instantiate(button2) as GameObject;
        three = Instantiate(button3) as GameObject;
        four = Instantiate(button4) as GameObject;
        five = Instantiate(button5) as GameObject;
        six = Instantiate(button6) as GameObject;
        seven = Instantiate(button7) as GameObject;
        eight = Instantiate(button8) as GameObject;
        nine = Instantiate(button9) as GameObject;
        print("Enter six digit code.");
    }
    public void OpenSafe()
    {
        if (count == 6)
        {
            if (combo == "312211")
            {
                safeOpen = true;
                Destroy(zoomed.gameObject);
                Destroy(zero.gameObject);
                Destroy(one.gameObject);
                Destroy(two.gameObject);
                Destroy(three.gameObject);
                Destroy(four.gameObject);
                Destroy(five.gameObject);
                Destroy(six.gameObject);
                Destroy(seven.gameObject);
                Destroy(eight.gameObject);
                Destroy(nine.gameObject);
                key = Instantiate(scene3Objects[6]) as GameObject;
            }
            else
            {
                print("Failed");
                combo = "";
                count = 0;
                Destroy(zoomed.gameObject);
                Destroy(zero.gameObject);
                Destroy(one.gameObject);
                Destroy(two.gameObject);
                Destroy(three.gameObject);
                Destroy(four.gameObject);
                Destroy(five.gameObject);
                Destroy(six.gameObject);
                Destroy(seven.gameObject);
                Destroy(eight.gameObject);
                Destroy(nine.gameObject);
            }
        }
        else
        {
            //do nothing
        }
    }
}
