﻿using UnityEngine;
using System.Collections;

public class ClockController : MonoBehaviour {
    public Sprite sprite1;
    public Sprite sprite2;
    SpriteRenderer spriteRenderer;
    public bool opened = false;
    public GameObject text;
    public GameObject text2;
    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(2.94f, 2.46f, 1.0f);
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
    void OnMouseUp()
    {
        if (GameController.S.hasScrewdriver)
        {
            if (!GameController.S.hasBlackLight && !opened)
            {
                opened = true;
                GameController.S.printText(text);
                print("Used the screwdriver to open the clock. You see a blacklight hidden inside...");
                Instantiate(GameController.S.scene2Objects[2]);
                spriteRenderer.sprite = sprite2;
            }
        }
        else
        {
            GameController.S.printText(text2);
            print("Nice clock.");
        }
    }
}
