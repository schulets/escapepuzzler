﻿using UnityEngine;
using System.Collections;

public class RightArrowController : MonoBehaviour {
    public int oldIndex;
    public int currentIndex;
    public GameObject text;

    void Start()
    {
        this.transform.position = new Vector3(6f, 0f, 0f);
    }

    public void OnMouseUp()
    {
        oldIndex = GameController.S.sceneIndex;
        currentIndex = GameController.S.sceneIndex;
        //if scene is even, lights are on
        if (currentIndex % 2 == 0)
        {
            //black light is off
            if (!GameController.S.blackLight)
            {
                if (currentIndex == 0)
                {
                    currentIndex = 4;
                    
                    //                    currentScene = scenes[4];

                }
                //if door is closed
                else if (!GameController.S.doorOpen)
                {
                    if (currentIndex == 4)
                    {
                        currentIndex = 6;
                        //                        currentScene = scenes[6];
                    }
                    else if (currentIndex == 6)
                    {
                        currentIndex = 10;
                        //                    currentScene = scenes[10];
                    }
                    else if (currentIndex == 10)
                    {
                        currentIndex = 0;
                        //                    currentScene = scenes[0];
                    }
                }
                //door is open
                else if (GameController.S.doorOpen && currentIndex == 4)
                {
                    currentIndex = 8;
                    //                    currentScene = scenes[8];
                }

                else if (currentIndex == 8)
                {
                    currentIndex = 10;
                    //                    currentScene = scenes[10];
                }
                else if (currentIndex == 10)
                {
                    currentIndex = 0;
                    //                    currentScene = scenes[0];
                }
            }
            //black light is on
            else if (GameController.S.blackLight)
            {
                if (currentIndex == 2)
                {
                    currentIndex = 4;
                    //                    currentScene = scenes[4];
                }
                //if door is closed
                else if (!GameController.S.doorOpen)
                {
                    if (currentIndex == 4)
                    {
                        currentIndex = 6;
                        //                        currentScene = scenes[6];
                    }
                    else if (currentIndex == 6)
                    {
                        currentIndex = 10;
                        //                    currentScene = scenes[10];
                    }

                    else if (currentIndex == 10)
                    {
                        currentIndex = 2;
                        //                    currentScene = scenes[2];
                    }
                }
                //door is open
                else if (GameController.S.doorOpen && currentIndex == 4)
                {
                    currentIndex = 8;
                    //                    currentScene = scenes[8];
                }
                else if (currentIndex == 8)
                {
                    currentIndex = 10;
                    //                    currentScene = scenes[10];
                }
                else if (currentIndex == 10)
                {
                    currentIndex = 2;
                    //                    currentScene = scenes[2];
                }
            }
        }
        //odd number, the lights are off
        else
        {
            //black light is off
            if (!GameController.S.blackLight)
            {
                if (currentIndex == 1)
                {
                    currentIndex = 5;
                    //                    currentScene = scenes[5];
                }
                //if door is closed
                else if (!GameController.S.doorOpen)
                {
                    if (currentIndex == 5)
                    {
                        currentIndex = 7;
                        //                        currentScene = scenes[7];
                    }
                    else if (currentIndex == 7)
                    {
                        currentIndex = 11;
                        //                    currentScene = scenes[11];
                    }
                    else if (currentIndex == 11)
                    {
                        currentIndex = 1;
                        //                    currentScene = scenes[1];
                    }
                }
                //door is open
                else if (GameController.S.doorOpen && currentIndex == 5)
                {
                    currentIndex = 9;
                    //                    currentScene = scenes[9];
                }

                else if (currentIndex == 9)
                {
                    currentIndex = 11;
                    //                    currentScene = scenes[11];
                }
                else if (currentIndex == 11)
                {
                    currentIndex = 1;
                    //                    currentScene = scenes[1];
                }
            }
            //black light is on
            else if (GameController.S.blackLight)
            {
                if (currentIndex == 3)
                {
                    currentIndex = 5;
                    //                    currentScene = scenes[5];
                }
                //if door is closed
                else if (!GameController.S.doorOpen)
                {
                    if (currentIndex == 5)
                    {
                        currentIndex = 7;
                        //                        currentScene = scenes[7];
                    }
                    else if (currentIndex == 7)
                    {
                        currentIndex = 11;
                    }
                    else if (currentIndex == 11)
                    {
                        currentIndex = 3;
                    }
                }
                //door is open
                else if (GameController.S.doorOpen && currentIndex == 5)
                {
                    currentIndex = 9;
                }

                else if (currentIndex == 9)
                {
                    currentIndex = 11;
                }
                else if (currentIndex == 11)
                {
                    currentIndex = 3;
                    
                }
            }
        }
        GameController.S.ChangeScene(oldIndex, currentIndex);
    }
}
