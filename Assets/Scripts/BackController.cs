﻿using UnityEngine;
using System.Collections;

public class BackController : MonoBehaviour {

	void OnMouseUp()
    {
        GameController.S.clicked = false;
        Destroy(GameController.S.zoomed);
        Destroy(GameController.S.backButton);
    }
}
