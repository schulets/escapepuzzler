﻿using UnityEngine;
using System.Collections;

public class ClosetController : MonoBehaviour {
    public Sprite sprite1;
    public Sprite sprite2;
    public SpriteRenderer spriteRenderer;
    public GameObject text;
    public GameObject text2;

    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (GameController.S.closetOpen)
        {
            spriteRenderer.sprite = sprite2;
        }
        else
        {
            spriteRenderer.sprite = sprite1;
        }
        this.transform.position = new Vector3(1.41f, 0.39f, 1.0f);
	}
	
    void OnMouseUp()
    {
        if(!GameController.S.closetOpen) {
            GameController.S.printText(text);
            print("You opened the closet. There is a crow bar inside...");
            spriteRenderer.sprite = sprite2;
            GameController.S.crowBar = Instantiate(GameController.S.scene4Objects[4]);
            GameController.S.closetOpen = true;
        }
        else
        {
            GameController.S.printText(text2);
            print("There is nothing else inside.");
        }
    }
}
