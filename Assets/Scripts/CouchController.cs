﻿using UnityEngine;
using System.Collections;

public class CouchController : MonoBehaviour {
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(2.95f, -3.05f, 1.0f);
	}
	
    void OnMouseUp()
    {
        GameController.S.printText(text);
        print("There was nothing in the couch. Looks comfortable though.");
    }
}
