﻿using UnityEngine;
using System.Collections;

public class SafeController : MonoBehaviour {
    public Sprite sprite1;
    public Sprite sprite2;
    public SpriteRenderer spriteRenderer;
    public GameObject text;

    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (!GameController.S.safeOpen)
        {
            spriteRenderer.sprite = sprite1;
        }
        else
        {
            spriteRenderer.sprite = sprite2;
        }
        this.transform.position = new Vector3(2.12f, 1.17f, 1.0f);
	}
	
    void OnMouseUp()
    {
        if (GameController.S.safeOpen)
        {
            GameController.S.printText(text);
            print("There is nothing more in the safe.");
        }
        else
        {
            GameController.S.zoomSafe();
        }
    }

    void Update()
    {
        if (GameController.S.safeOpen)
        {
            spriteRenderer.sprite = sprite2;
        }
    }
}
