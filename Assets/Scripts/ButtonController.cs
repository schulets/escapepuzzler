﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
    public int value;

	// Use this for initialization
	void Start () {
	}

    void OnMouseUp()
    {
        GameController.S.count++;
        GameController.S.combo += value;
        print(GameController.S.combo);
        GameController.S.OpenSafe();
    }
}
