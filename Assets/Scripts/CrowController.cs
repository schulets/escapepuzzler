﻿using UnityEngine;
using System.Collections;

public class CrowController : MonoBehaviour {

    public Sprite text;
    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(2.19f, -1.74f, 0.0f);
	}
	
    void OnMouseUp()
    {
        GameController.S.hasCrowbar = true;
        Destroy(this.gameObject);
    }
}
