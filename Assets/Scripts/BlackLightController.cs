﻿using UnityEngine;
using System.Collections;

public class BlackLightController : MonoBehaviour {
    public GameObject text;

    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3(2.95f, 0.82f, 1.0f);
	}

    void OnMouseUp()
    {
        GameController.S.hasBlackLight = true;
        Destroy(this.gameObject);
    }
}
