﻿using UnityEngine;
using System.Collections;

public class PaintingController : MonoBehaviour {
    public Sprite sprite1;
    public Sprite sprite2;

    public GameObject text;
    public SpriteRenderer spriteRenderer;
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (!GameController.S.lightsOn && GameController.S.blackLight)
        {
            spriteRenderer.sprite = sprite2;
        }
        else
        {
            spriteRenderer.sprite = sprite1;
        }
        this.transform.position = new Vector3(2.58f, 2.82f, 1f);
    }

    void Update()
    {
        if (!GameController.S.lightsOn && GameController.S.blackLight)
        {
            spriteRenderer.sprite = sprite2;
        }
        else
        {
            spriteRenderer.sprite = sprite1;
        }
    }

    void OnMouseUp() {

        if (!GameController.S.lightsOn && GameController.S.blackLight)
        {
            //print("Hey, it's a puzzle!");
            GameController.S.clicked = true;
            GameController.S.zoomPainting();
        }
        else
        {
            GameController.S.printText(text);
            print("Nice painting.");
        }
    }
}
